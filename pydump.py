#!/usr/bin/env python

import os
import sys
import datetime
import time

from stat import *

# mysqlbackup user
User = 'root'

# mysqlbackup password
Password = 'root'

# mysqlbackup command
Mysqlcommand = '/usr/local/mysql/bin/mysqldump'

# gzip command
Gzipcommand = '/bin/gzip'

# backup mysql database
Mysqldata = ['gtshop']

# backup dir
Tobackup = '/home/gtshop_backup/'

for DB in Mysqldata:
    # backup file name
    Backupfile = Tobackup + DB + '-' + time.strftime('%Y-%m-%d') + '.sql'
    # gzip file name
    Gzipfile = Backupfile + '.gz'
    if os.path.isfile(Gzipfile):
        print Gzipfile + "is already backup"
    else:
        # backup command
        Back_command = Mysqlcommand + ' -u' + User + ' -p' + Password + ' --events ' + \
            ' --master-data=2 ' + ' --single-transaction ' + DB + ' > ' + Backupfile
        if os.system(Back_command) == 0:
            print 'Sucessful backup gtshop'
        else:
            print 'Backup failed'
    # gzip command
        Gzip_command = Gzipcommand + ' ' + Backupfile
        if os.system(Gzip_command) == 0:
            print 'sucessful gzip gtshop'
        else:
            print 'gzip failed'
